- [ ] Faire un fichier qui lit les fichiers pour le convertir en règle et
      qui lance le résolveur
- [ ] Faire le résolveur

Le prof propose d'[utiliser C/C++ pour utiliser Prolog](./AidesCPP/)
avec `swi-prolog-devel`
([header](https://www.swi-prolog.org/pldoc/man?section=foreignlink) :
`SWI-Prolog.h` ou `SWI-cpp.h`)
