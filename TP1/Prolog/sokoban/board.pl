next_location(Start, End, up) :- top(Start, End).
next_location(Start, End, down) :- top(End, Start).
next_location(Start, End, right) :- top(Start, End).
next_location(Start, End, left) :- top(End, Start).

% explication nécessaire
corner(X) :- \+ noncorner(X).
noncorner(X) :- top(_, X), top(X, _).
noncorner(X) :- right(_, X), right(X, _).

% explication nécessaire
stuck(X, Y) :- 
    (right(X, Y) ; right(Y, X)),
    (\+ storage(X); \+ storage(Y)),
    (\+ top(X,_), \+ top(Y,_); \+ top(_,X), \+ top(_,Y)),
    !.
stuck(X,Y):-
    (top(X,Y);top(Y,X)),
    (\+ storage(X); \+ storage(Y)),
    (\+ right(X,_), \+ right(Y,_); \+ right(_,X), \+ right(_,Y)),
    !.

insert_list(X, [], [X]).
insert_list(X, [Y|Ysuite], [X, Y|Ysuite]) :-
    X @=<Y,!.
insert_list(X, [Y|Ysuite], [Y|Ordered]) :-
    insert_list(X, Ysuite, Ordered).

end([]).
end([Pos|Suite]) :-
    storage(Pos),
    end(Suite).
