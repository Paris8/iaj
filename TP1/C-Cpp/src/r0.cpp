#include "../includes/mysok.h"

int main(int _ac, char **_av) {
  if (_ac != 2) {
    printf("usage: %s SOK_FILE\n", _av[0]);

    return 0;
  }

  sok_board_t S;
  S.load(_av[1]);
  S.print_board();

  Node result;

  result.state = S;
  result.path = "";
  result.path_len = 0;
  /* if (bfs(result) == -1) {
    std::cout << "Aucune solution trouvée\n";
  } else {
    std::cout << "Coups : " << result.path_len << "\n";
    std::cout << "Solution : " << result.path << "\n";
  } */

  std::vector<Node> history;

  if (dfs(result, 0, history) == -1) {
    std::cout << "Aucune solution trouvée\n";
  } else {
    std::cout << "Coups : " << result.path_len << "\n";
    std::cout << "Solution : " << result.path << "\n";
  }

  // result.state.print_board();

  return 0;
}
