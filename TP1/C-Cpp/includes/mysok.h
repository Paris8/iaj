#ifndef MYSOK_H
#define MYSOK_H

#include <iostream>
#include <queue>
#include <stack>
#include <string>

#define NBL 20
#define NBC 20
#define MAX_DEPTH 60

enum movement { MOVE_U = 0, MOVE_D, MOVE_L, MOVE_R, MOVE_W };

enum board_str {
  OUT = ' ',
  FREE = '_',
  TARGET = '.',
  WALL = '#',
  CRATE_ON_FREE = '$',
  CRATE_ON_TARGET = '*',
  MAN1_ON_FREE = '1',
  MAN1_ON_TARGET = 'u',
  MAN2_ON_FREE = '2',
  MAN2_ON_TARGET = 'd',
  END_OF_LINE = 'a'
};

const std::string move_str[] = {"Up", "Down", "Left", "Right", "Wait"};

const int dx[] = {-1, 1, 0, 0, 0};
const int dy[] = {0, 0, -1, 1, 0};
const int dsize = 5;

struct sok_board_t {
  int board[NBL][NBC];
  int board_nbl;
  int man1_x;
  int man1_y;
  int man2_x;
  int man2_y;
  int num_crates_free;

  sok_board_t();
  void print_board();
  void load(char *_file);
};

typedef struct {
  sok_board_t state;
  int path_len;
  std::string path;
} Node;

int bfs(Node &result);
int dfs(Node &result, int depth, std::vector<Node> history);

#endif
