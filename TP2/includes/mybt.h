#ifndef MYBT_H
#define MYBT_H

#include <chrono>
#include <random>

#define WHITE 0
#define BLACK 1
#define EMPTY 2

struct bt_piece_t {
  int line;
  int col;
};

struct bt_move_t {
  int line_i;
  int col_i;
  int line_f;
  int col_f;

  // all moves are printed without ambiguity
  // white in its color
  // black in red color
  void print(FILE *_fp, bool _white, int _nbl) {
    if (_white) {
      fprintf(_fp, "%d%c%d%c", _nbl - line_i, 'a' + col_i, _nbl - line_f,
              'a' + col_f);
    } else {
      fprintf(_fp, "\x1B[31m%d%c%d%c\x1B[0m", _nbl - line_i, 'a' + col_i,
              _nbl - line_f, 'a' + col_f);
    }
  }
  std::string tostr(int _nbl) {
    char ret[16];
    snprintf(ret, sizeof(ret), "%d%c%d%c", _nbl - line_i, 'a' + col_i,
             _nbl - line_f, 'a' + col_f);
    return std::string(ret);
  }
};

// alloc default 10x10
// standard game in 8x8
#define MAX_LINES 10
#define MAX_COLS 10

// Node MCTS
struct bt_node_t {
  bt_node_t *parent;
  std::vector<bt_node_t *> children;
  bt_move_t move;
  int wins;
  int nb_simulation;
};

// rules reminder :
// pieces moves from 1 square in diag and in front
// pieces captures only in diag
// i.e. to go forward, square must be empty
struct bt_t {
  char *cboard = const_cast<char *>("o@.");

  int nbl;
  int nbc;
  int board[MAX_LINES][MAX_COLS];
  int turn;

  bt_piece_t white_pieces[2 * MAX_LINES];
  int nb_white_pieces;
  bt_piece_t black_pieces[2 * MAX_LINES];
  int nb_black_pieces;
  bt_move_t moves[3 * 2 * MAX_LINES];
  int nb_moves;

  // last turn of moves update
  int turn_of_last_moves_update;

  void init(int _nbl, int _nbc);
  void init_pieces();
  void print_board(FILE *_fp);
  void print_turn_and_moves(FILE *_fp);
  void update_moves();
  void update_moves(int _color);

  bool white_can_move_right(int _line, int _col);
  bool white_can_move_forward(int _line, int _col);
  bool white_can_move_left(int _line, int _col);
  bool black_can_move_right(int _line, int _col);
  bool black_can_move_forward(int _line, int _col);
  bool black_can_move_left(int _line, int _col);

  bt_move_t get_mcts_move(double);
  bt_move_t get_rand_move();
  bool can_play(bt_move_t _m);
  void play(bt_move_t _m);
  int endgame();
  double score(int _color);
  void playout(bool _log);
  std::string mkH1();
  std::string mkH2();
  long long int mkH3();

  // MCTS
  bt_node_t *mcts_selection(bt_node_t *);
  void mcts_expansion(bt_node_t *);
  bool mcts_simulation();
  static void mcts_back_propagation(bt_node_t *, bool);

  void add_move(int _li, int _ci, int _lf, int _cf) {
    moves[nb_moves].line_i = _li;
    moves[nb_moves].col_i = _ci;
    moves[nb_moves].line_f = _lf;
    moves[nb_moves].col_f = _cf;
    nb_moves++;
  }
};

#endif /* MYBT_H */
