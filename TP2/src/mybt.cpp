#include "../includes/mybt.h"

void bt_t::init(int _nbl, int _nbc) {
  if (_nbl > MAX_LINES || _nbc > MAX_COLS) {
    fprintf(stderr, "ERROR : MAX_LINES or MAX_COLS exceeded\n");
    exit(0);
  }
  nbl = _nbl;
  nbc = _nbc;
  turn = 0;
  turn_of_last_moves_update = -1;
  for (int i = 0; i < nbl; i++)
    for (int j = 0; j < nbc; j++) {
      if (i <= 1) {
        board[i][j] = BLACK;
      } else if (i < _nbl - 2) {
        board[i][j] = EMPTY;
      } else {
        board[i][j] = WHITE;
      }
    }
  init_pieces();
  update_moves();
}

void bt_t::init_pieces() {
  nb_white_pieces = 0;
  nb_black_pieces = 0;
  for (int i = 0; i < nbl; i++)
    for (int j = 0; j < nbc; j++) {
      if (board[i][j] == WHITE) {
        white_pieces[nb_white_pieces].line = i;
        white_pieces[nb_white_pieces].col = j;
        nb_white_pieces++;
      } else if (board[i][j] == BLACK) {
        black_pieces[nb_black_pieces].line = i;
        black_pieces[nb_black_pieces].col = j;
        nb_black_pieces++;
      }
    }
}

// again print black in red (as bg is black... black is printed in red)
void bt_t::print_board(FILE *_fp = stderr) {
  fprintf(_fp, "   \x1B[34m");
  for (int j = 0; j < nbc; j++) {
    fprintf(_fp, "%c ", 'a' + j);
  }
  fprintf(_fp, "\x1B[0m\n");
  for (int i = 0; i < nbl; i++) {
    fprintf(_fp, "\x1B[34m%2d\x1B[0m ", (nbl - i));
    for (int j = 0; j < nbc; j++) {
      if (board[i][j] == BLACK) {
        fprintf(_fp, "\x1B[31m%c\x1B[0m ", cboard[board[i][j]]);
      } else {
        fprintf(_fp, "%c ", cboard[board[i][j]]);
      }
    }
    fprintf(_fp, "\n");
  }
}

void bt_t::print_turn_and_moves(FILE *_fp = stderr) {
  fprintf(_fp, "turn:%d\nmoves:", turn);
  for (int i = 0; i < nb_moves; i++) {
    moves[i].print(_fp, turn % 2 == 1, nbl);
    fprintf(_fp, " ");
  }
  fprintf(_fp, "\n");
}

void bt_t::update_moves() {
  if (turn % 2 == 0) {
    update_moves(WHITE);
  } else {
    update_moves(BLACK);
  }
}

void bt_t::update_moves(int _color) {
  if (turn_of_last_moves_update == turn) {
    return; // MAJ ever done
  }
  turn_of_last_moves_update = turn;
  nb_moves = 0;
  if (_color == WHITE) {
    for (int i = 0; i < nb_white_pieces; i++) {
      int li = white_pieces[i].line;
      int ci = white_pieces[i].col;
      if (white_can_move_right(li, ci)) {
        add_move(li, ci, li - 1, ci + 1);
      }
      if (white_can_move_forward(li, ci)) {
        add_move(li, ci, li - 1, ci);
      }
      if (white_can_move_left(li, ci)) {
        add_move(li, ci, li - 1, ci - 1);
      }
    }
  } else if (_color == BLACK) {
    for (int i = 0; i < nb_black_pieces; i++) {
      int li = black_pieces[i].line;
      int ci = black_pieces[i].col;
      if (black_can_move_right(li, ci)) {
        add_move(li, ci, li + 1, ci + 1);
      }
      if (black_can_move_forward(li, ci)) {
        add_move(li, ci, li + 1, ci);
      }
      if (black_can_move_left(li, ci)) {
        add_move(li, ci, li + 1, ci - 1);
      }
    }
  }
}

bool bt_t::white_can_move_right(int _line, int _col) {
  if (_line == 0) {
    return false;
  }
  if (_col == nbc - 1) {
    return false;
  }
  if (board[_line - 1][_col + 1] != WHITE) {
    return true;
  }
  return false;
}

bool bt_t::white_can_move_forward(int _line, int _col) {
  if (_line == 0) {
    return false;
  }
  if (board[_line - 1][_col] == EMPTY) {
    return true;
  }
  return false;
}

bool bt_t::white_can_move_left(int _line, int _col) {
  if (_line == 0) {
    return false;
  }
  if (_col == 0) {
    return false;
  }
  if (board[_line - 1][_col - 1] != WHITE) {
    return true;
  }
  return false;
}

bool bt_t::black_can_move_right(int _line, int _col) {
  if (_line == nbl - 1) {
    return false;
  }
  if (_col == nbc - 1) {
    return false;
  }
  if (board[_line + 1][_col + 1] != BLACK) {
    return true;
  }
  return false;
}

bool bt_t::black_can_move_forward(int _line, int _col) {
  if (_line == nbl - 1) {
    return false;
  }
  if (board[_line + 1][_col] == EMPTY) {
    return true;
  }
  return false;
}

bool bt_t::black_can_move_left(int _line, int _col) {
  if (_line == nbl - 1) {
    return false;
  }
  if (_col == 0) {
    return false;
  }
  if (board[_line + 1][_col - 1] != BLACK) {
    return true;
  }
  return false;
}

bt_move_t bt_t::get_rand_move() {
  update_moves();
  int r = (static_cast<int>(rand())) % nb_moves;
  return moves[r];
}

bt_node_t *bt_t::mcts_selection(bt_node_t *node) {
  // Play the move is game over or first call
  if ((endgame() != EMPTY) ||
      ((node->nb_simulation == 0) && (node->parent != nullptr))) {
    play(node->move);
    return node;
  }

  bt_node_t *best = nullptr;
  float score = std::numeric_limits<float>::lowest();
  for (auto i : node->children) {
    // Play the move if never played
    if (i->nb_simulation == 0) {
      play(i->move);

      return i;
    }

    // https://en.wikipedia.org/wiki/Monte_Carlo_tree_search#Exploration_and_exploitation
    float curr_score =
        (static_cast<float>(i->wins) / i->nb_simulation) +
        ((sqrt(2) * sqrt(log(node->nb_simulation)) / i->nb_simulation));

    if (curr_score > score) {
      best = i;
      score = curr_score;
    }
  }

  play(best->move);
  return mcts_selection(best);
}

void bt_t::mcts_expansion(bt_node_t *node) {
  update_moves();

  for (int i = 0; i < nb_moves; i++) {
    // Child
    bt_node_t *tmp = new bt_node_t;
    tmp->move = moves[i];
    tmp->wins = 0;
    tmp->parent = node;
    tmp->nb_simulation = 0;

    // Add child to selected node
    node->children.push_back(tmp);
  }
}

bool bt_t::mcts_simulation(void) {
  int me = (turn % 2 == 0) ? WHITE : BLACK;

  // then play randomly 'til the game is over
  while (endgame() == EMPTY) {
    update_moves();
    if (nb_moves > 0) {
      int r = (static_cast<int>(rand())) % nb_moves;
      bt_move_t m = moves[r];
      play(m);
    } else {
      break;
    }
  }

  // if i won
  return ((turn % 2 == 0) ? WHITE : BLACK) == me;
}

void bt_t::mcts_back_propagation(bt_node_t *simulated, bool won) {
  // propagate values to the top of the tree
  while (simulated != nullptr) {
    if (won) {
      simulated->wins++;
    }
    simulated->nb_simulation++;
    simulated = simulated->parent;
  }
}

void print_vec(bt_t board, std::vector<bt_node_t *> v) {
  if (v.empty()) {
    fprintf(stderr, "[]\n");
    return;
  }
  for (auto i : v) {
    fprintf(stderr, "move(%s)", i->move.tostr(board.nbl).c_str());
    fprintf(stderr, " label(%d/%d)", i->wins, i->nb_simulation);
    fprintf(stderr, " parent(%s)\n", i->parent->move.tostr(board.nbl).c_str());
  }
}

bt_move_t bt_t::get_mcts_move(double max_time) {
  // Time constraint
  auto now = std::chrono::steady_clock::now();
  std::chrono::duration<double> elapsed{};

  // Init tree
  bt_node_t *tree = new bt_node_t();
  tree->parent = nullptr;
  tree->children = {};
  tree->wins = 0;
  tree->nb_simulation = 0;

  // On remplie l'arbre avec les entrées de départ
  mcts_expansion(tree);

  // MCTS
  while (elapsed.count() < max_time) {
    // Copy board
    bt_t copy_b = *this;

    // Selection
    bt_node_t *result = copy_b.mcts_selection(tree);

    // Expansion
    copy_b.mcts_expansion(result);

    // Simulation
    bool is_win = copy_b.mcts_simulation();

    // Update
    mcts_back_propagation(result, is_win);

    // Update elapsed time
    elapsed = std::chrono::steady_clock::now() - now;
  }

  // Select best move
  bt_node_t *best_node = nullptr;
  int best_score = 0;
  for (auto i : tree->children) {
    if (i->nb_simulation > best_score) {
      best_score = i->nb_simulation;
      best_node = i;
    }
  }
  return best_node->move;
}

bool bt_t::can_play(bt_move_t _m) {
  int dx = abs(_m.col_f - _m.col_i);
  if (dx > 1) {
    return false;
  }
  int dy = abs(_m.line_f - _m.line_i);
  if (dy > 1) {
    return false;
  }
  if (_m.line_i < 0 || _m.line_i >= nbl) {
    return false;
  }
  if (_m.line_f < 0 || _m.line_f >= nbl) {
    return false;
  }
  if (_m.col_i < 0 || _m.col_i >= nbc) {
    return false;
  }
  if (_m.col_f < 0 || _m.col_f >= nbc) {
    return false;
  }
  int color_i = board[_m.line_i][_m.col_i];
  int color_f = board[_m.line_f][_m.col_f];
  if (color_i == EMPTY) {
    return false;
  }
  if (color_i == color_f) {
    return false;
  }
  if (turn % 2 == 0 && color_i == BLACK) {
    return false;
  }
  if (turn % 2 == 1 && color_i == WHITE) {
    return false;
  }
  if (_m.col_i == _m.col_f && color_f != EMPTY) {
    return false;
  }
  return true;
}

void bt_t::play(bt_move_t _m) {
  int color_i = board[_m.line_i][_m.col_i];
  int color_f = board[_m.line_f][_m.col_f];
  board[_m.line_f][_m.col_f] = color_i;
  board[_m.line_i][_m.col_i] = EMPTY;
  if (color_i == WHITE) {
    for (int i = 0; i < nb_white_pieces; i++) {
      if (white_pieces[i].line == _m.line_i &&
          white_pieces[i].col == _m.col_i) {
        white_pieces[i].line = _m.line_f;
        white_pieces[i].col = _m.col_f;
        break;
      }
    }
    if (color_f == BLACK) {
      for (int i = 0; i < nb_black_pieces; i++) {
        if (black_pieces[i].line == _m.line_f &&
            black_pieces[i].col == _m.col_f) {
          black_pieces[i] = black_pieces[nb_black_pieces - 1];
          nb_black_pieces--;
          break;
        }
      }
    }
  } else if (color_i == BLACK) {
    for (int i = 0; i < nb_black_pieces; i++) {
      if (black_pieces[i].line == _m.line_i &&
          black_pieces[i].col == _m.col_i) {
        black_pieces[i].line = _m.line_f;
        black_pieces[i].col = _m.col_f;
        break;
      }
    }
    if (color_f == WHITE) {
      for (int i = 0; i < nb_white_pieces; i++) {
        if (white_pieces[i].line == _m.line_f &&
            white_pieces[i].col == _m.col_f) {
          white_pieces[i] = white_pieces[nb_white_pieces - 1];
          nb_white_pieces--;
          break;
        }
      }
    }
  }
  turn++;
}

int bt_t::endgame() {
  if (nb_black_pieces == 0) {
    return WHITE;
  }
  if (nb_white_pieces == 0) {
    return BLACK;
  }

  for (int i = 0; i < nbc; i++) {
    if (board[0][i] == WHITE) {
      return WHITE;
    }
  }
  for (int i = 0; i < nbc; i++) {
    if (board[nbl - 1][i] == BLACK) {
      return BLACK;
    }
  }
  return EMPTY;
}

double bt_t::score(int _color) {
  int state = endgame();
  if (state == EMPTY) {
    return 0.0;
  }
  if (_color == state) {
    return 1.0;
  }
  return -1.0;
}
